from fastapi import APIRouter, Depends

from . import controllers, schemas

details = APIRouter(
    prefix="/details",
    tags=["details"],
    responses={404: {"description": "Not found"}},
)


@details.post("/", response_model=schemas.Details)
def create_detail(detail: schemas.CreateDetails = Depends(controllers.add_detail)):
    return detail
