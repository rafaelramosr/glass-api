from pydantic import BaseModel


# TO support creation APIs
class CreateDetails(BaseModel):
    descripcion: str
    ancho: float
    alto: float
    orden_id: int


# TO support list and get APIs
class Details(CreateDetails):
    id: int

    class Config:
        orm_mode = True
