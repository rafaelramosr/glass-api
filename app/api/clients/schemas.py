from pydantic import BaseModel
from typing import List


# TO support creation and update APIs
class CreateAndUpdateClient(BaseModel):
    nombre: str
    direccion: str
    telefono: int
    correo: str
    pais_id: int


# TO support list and get APIs
class Client(CreateAndUpdateClient):
    id: int

    class Config:
        orm_mode = True


# To support list cars API
class PaginatedClientInfo(BaseModel):
    limit: int
    offset: int
    data: List[Client]
