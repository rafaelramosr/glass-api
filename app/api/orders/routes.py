from fastapi import APIRouter, Depends

from . import controllers, schemas

orders = APIRouter(
    prefix="/orders",
    tags=["orders"],
    responses={404: {"description": "Not found"}},
)


@orders.post("/", response_model=schemas.Orders)
def create_order(order: schemas.Orders = Depends(controllers.add_order)):
    return order


@orders.put("/{orden_id}", response_model=schemas.Orders)
def edit_order(order: schemas.Orders = Depends(controllers.update_order)):
    return order
