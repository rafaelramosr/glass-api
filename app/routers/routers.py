from fastapi import APIRouter

from ..api.clients.routes import clients
from ..api.details.routes import details
from ..api.orders.routes import orders

router = APIRouter()

router.include_router(clients)
router.include_router(details)
router.include_router(orders)
