from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from . import ops, schemas, exceptions
from ...db.database import get_db

router = APIRouter()


def add_detail(detail_data: schemas.CreateDetails, session: Session = Depends(get_db)):
    try:
        detail_data = ops.create_detail(session, detail_data)
        return detail_data
    except exceptions.DetailsInfoAlreadyExistError as cie:
        raise HTTPException(**cie.__dict__)
