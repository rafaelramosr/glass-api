from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from . import ops
from ...db.database import get_db
from .exceptions import ClientInfoException
from .schemas import CreateAndUpdateClient

router = APIRouter()


def list_clients(limit: int = 10, offset: int = 0, session: Session = Depends(get_db)):
    clients_list = ops.get_all_clients(session, limit, offset)
    response = {"limit": limit, "offset": offset, "data": clients_list}

    return response


def add_client(client_info: CreateAndUpdateClient, session: Session = Depends(get_db)):
    try:
        client_info = ops.create_client(session, client_info)
        return client_info
    except ClientInfoException as cie:
        raise HTTPException(**cie.__dict__)


def get_client_info(cliente_id: int, session: Session = Depends(get_db)):
    try:
        client_info = ops.get_client_info_by_id(session, cliente_id)
        return client_info
    except ClientInfoException as cie:
        raise HTTPException(**cie.__dict__)


def update_client(
    cliente_id: int, new_info: CreateAndUpdateClient, session: Session = Depends(get_db)
):
    try:
        client_info = ops.update_client_info(session, cliente_id, new_info)
        return client_info
    except ClientInfoException as cie:
        raise HTTPException(**cie.__dict__)


def delete_client(cliente_id: int, session: Session = Depends(get_db)):
    try:
        return ops.delete_client_info(session, cliente_id)
    except ClientInfoException as cie:
        raise HTTPException(**cie.__dict__)
