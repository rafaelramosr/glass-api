from sqlalchemy.schema import Column
from sqlalchemy.sql.sqltypes import String, DECIMAL
from sqlalchemy.types import Integer

from ...db.database import Base


class Details(Base):
    __tablename__ = "detalle"

    id = Column(Integer, primary_key=True, index=True)
    descripcion = Column(String(250), nullable=False)
    ancho = Column(DECIMAL(precision=5, scale=2), nullable=False)
    alto = Column(DECIMAL(precision=5, scale=2), nullable=False)
    orden_id = Column(Integer, nullable=False)
