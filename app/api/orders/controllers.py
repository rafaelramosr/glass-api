from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from . import ops
from ...db.database import get_db
from .exceptions import OrderInfoException
from .schemas import CreateOrders, UpdateOrders

router = APIRouter()


def add_order(order_info: CreateOrders, session: Session = Depends(get_db)):
    try:
        order_info = ops.create_order(session, order_info)
        return order_info
    except OrderInfoException as cie:
        raise HTTPException(**cie.__dict__)


def update_order(
    orden_id: int, new_info: UpdateOrders, session: Session = Depends(get_db)
):
    try:
        order_info = ops.update_order_info(session, orden_id, new_info)
        return order_info
    except OrderInfoException as cie:
        raise HTTPException(**cie.__dict__)
