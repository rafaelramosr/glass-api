from sqlalchemy.schema import Column
from sqlalchemy.types import String, Integer

from ...db.database import Base


class Clients(Base):
    __tablename__ = "cliente"

    id = Column(Integer, primary_key=True, index=True)
    nombre = Column(String(100), nullable=False)
    direccion = Column(String(150), nullable=False)
    telefono = Column(
        Integer,
        nullable=False,
    )
    correo = Column(String(100), nullable=False)
    pais_id = Column(Integer, nullable=False)
