from fastapi import FastAPI

from .routers import routers

# Initialize the app
app = FastAPI()

app.include_router(routers.router)


# GET operation at route '/'
@app.get("/")
async def root():
    return {"message": "Welcome to Glass API!"}
