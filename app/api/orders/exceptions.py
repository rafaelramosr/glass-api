class OrderInfoException(Exception):
    ...


class OrderInfoNotFoundError(OrderInfoException):
    def __init__(self):
        self.status_code = 404
        self.detail = "Order Info Not Found"


class OrderInfoAlreadyExistError(OrderInfoException):
    def __init__(self):
        self.status_code = 409
        self.detail = "Order Info Already Exists"
