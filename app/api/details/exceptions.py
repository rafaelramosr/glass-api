class DetailsInfoException(Exception):
    ...


class DetailsInfoAlreadyExistError(DetailsInfoException):
    def __init__(self):
        self.status_code = 409
        self.detail = "Details Info Already Exists"
