class ClientInfoException(Exception):
    ...


class ClientInfoNotFoundError(ClientInfoException):
    def __init__(self):
        self.status_code = 404
        self.detail = "Client Info Not Found"


class ClientInfoAlreadyExistError(ClientInfoException):
    def __init__(self):
        self.status_code = 409
        self.detail = "Client Info Already Exists"
