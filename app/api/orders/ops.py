from sqlalchemy.orm import Session

from .exceptions import OrderInfoAlreadyExistError, OrderInfoNotFoundError
from .models import Orders
from .schemas import CreateOrders, UpdateOrders


# Function to add a new order info to the database
def create_order(session: Session, order_info: CreateOrders) -> Orders:
    order_details = (
        session.query(Orders).filter(Orders.nro_orden == order_info.nro_orden).first()
    )
    if order_details is not None:
        raise OrderInfoAlreadyExistError

    new_order_info = Orders(**order_info.dict())
    session.add(new_order_info)
    session.commit()
    session.refresh(new_order_info)
    return new_order_info


# Function to  get info of a particular order
def get_order_info_by_id(session: Session, _id: int) -> Orders:
    order_info = session.query(Orders).get(_id)

    if order_info is None:
        raise OrderInfoNotFoundError

    return order_info


# Function to update details of the order
def update_order_info(session: Session, _id: int, info_update: UpdateOrders) -> Orders:
    order_info = get_order_info_by_id(session, _id)

    if order_info is None:
        raise OrderInfoNotFoundError

    order_info.estado = info_update.estado

    session.commit()
    session.refresh(order_info)

    return order_info
