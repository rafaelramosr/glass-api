from fastapi import APIRouter, Depends
from typing import List

from .schemas import Client, PaginatedClientInfo
from .controllers import (
    add_client,
    delete_client,
    get_client_info,
    list_clients,
    update_client,
)

clients = APIRouter(
    prefix="/clients",
    tags=["clients"],
    responses={404: {"description": "Not found"}},
)


@clients.delete("/{cliente_id}", response_model=Client)
def client_delete_one(client=Depends(delete_client)):
    return client


@clients.get("/", response_model=PaginatedClientInfo)
def client_get_all(all_clients: List[Client] = Depends(list_clients)):
    return all_clients


@clients.get("/{cliente_id}", response_model=Client)
def client_get_one(client: Client = Depends(get_client_info)):
    return client


@clients.post("/", response_model=Client)
def create_client(client: Client = Depends(add_client)):
    return client


@clients.put("/{cliente_id}", response_model=Client)
def edit_client(client: Client = Depends(update_client)):
    return client
