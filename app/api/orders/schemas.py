from pydantic import BaseModel
from datetime import date
from .models import StateTypes


# TO support creation APIs
class CreateOrders(BaseModel):
    nro_orden: int
    cliente_id: int
    fecha_orden: date
    estado: StateTypes


# TO support update APIs
class UpdateOrders(BaseModel):
    estado: StateTypes


# TO support list and get APIs
class Orders(CreateOrders):
    id: int

    class Config:
        orm_mode = True
