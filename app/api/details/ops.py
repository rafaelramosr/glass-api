from sqlalchemy.orm import Session

from . import models, schemas


# Function to add a new detail info to the database
def create_detail(
    session: Session, detail_info: schemas.CreateDetails
) -> schemas.Details:
    new_detail_info = models.Details(**detail_info.dict())
    session.add(new_detail_info)
    session.commit()
    session.refresh(new_detail_info)
    return new_detail_info
