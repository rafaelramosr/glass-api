from typing import List
from sqlalchemy.orm import Session

from .exceptions import ClientInfoAlreadyExistError, ClientInfoNotFoundError
from .models import Clients
from .schemas import CreateAndUpdateClient


# Function to get list of client info
def get_all_clients(session: Session, limit: int, offset: int) -> List[Clients]:
    return session.query(Clients).offset(offset).limit(limit).all()


# Function to  get info of a particular client
def get_client_info_by_id(session: Session, _id: int) -> Clients:
    client_info = session.query(Clients).get(_id)

    if client_info is None:
        raise ClientInfoNotFoundError

    return client_info


# Function to add a new client info to the database
def create_client(session: Session, client_info: CreateAndUpdateClient) -> Clients:
    client_details = (
        session.query(Clients).filter(Clients.correo == client_info.correo).first()
    )
    if client_details is not None:
        raise ClientInfoAlreadyExistError

    new_client_info = Clients(**client_info.dict())
    session.add(new_client_info)
    session.commit()
    session.refresh(new_client_info)
    return new_client_info


# Function to update details of the client
def update_client_info(
    session: Session, _id: int, info_update: CreateAndUpdateClient
) -> Clients:
    client_info = get_client_info_by_id(session, _id)

    if client_info is None:
        raise ClientInfoNotFoundError

    client_info.nombre = info_update.nombre
    client_info.direccion = info_update.direccion
    client_info.telefono = info_update.telefono
    client_info.correo = info_update.correo
    client_info.pais_id = info_update.pais_id

    session.commit()
    session.refresh(client_info)

    return client_info


# Function to delete a client info from the db
def delete_client_info(session: Session, _id: int) -> Clients:
    client_info = get_client_info_by_id(session, _id)

    if client_info is None:
        raise ClientInfoNotFoundError

    session.delete(client_info)
    session.commit()

    return client_info
