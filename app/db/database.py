from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from dotenv import load_dotenv
import os


load_dotenv()

MYSQL_ROOT_PASSWORD = os.environ.get("MYSQL_ROOT_PASSWORD")

DATABASE_URL = f"mysql+pymysql://root:{MYSQL_ROOT_PASSWORD}@localhost/glass"

db_engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=db_engine)

Base = declarative_base()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()
