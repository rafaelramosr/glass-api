from sqlalchemy.schema import Column
from sqlalchemy.types import Integer, Date, Enum

from ...db.database import Base
import enum


class StateTypes(str, enum.Enum):
    anulada = "anulada"
    aprobada = "aprobada"
    solicitada = "solicitada"


class Orders(Base):
    __tablename__ = "orden"

    id = Column(Integer, primary_key=True, index=True)
    nro_orden = Column(Integer, nullable=False)
    cliente_id = Column(Integer, nullable=False)
    fecha_orden = Column(
        Date,
        nullable=False,
    )
    estado = Column(Enum(StateTypes), nullable=False)
