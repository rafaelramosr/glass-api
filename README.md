# GLASS API 🧐
Rest API built in python with fastAPI

## Getting Started 🚀
1. Clone the repository:
```bash
git clone https://gitlab.com/rafaelramosr/glass-api.git
```

2. Enter the folder:
```bash
cd glass-api
```

3. Create virtual environment:
```bash
# Linux
python3 -m venv venv

# Windows
virtualenv venv
```

4. Activate virtual environment:
```bash
# Linux
source venv/bin/activate

# Windows
. \venv\Scripts\activate.bat
```

5. Install the dependencies:
```bash
pip install -r requirements.txt
```

6. Set env var `MYSQL_ROOT_PASSWORD`:
```bash
# Linux
export MYSQL_ROOT_PASSWORD="<root_password>"

# Windows
$Env:MYSQL_ROOT_PASSWORD = "<root_password>"
```

7. Run the local server and you're done:
```bash
uvicorn app.main:app --reload
```

8. Open your browser at:
  - [Home](http://127.0.0.1:8000/) - `http://127.0.0.1:8000/`
  - [Documentation](http://127.0.0.1:8000/docs) - `http://127.0.0.1:8000/docs`
  - [Alternative doc](http://127.0.0.1:8000/redoc) - `http://127.0.0.1:8000/redoc`

## Utility Commands 🔧

- Create `requirements.txt` file from already installed packages
  ```bash
  pip freeze > requirements.txt
  ```

- Create a database image with Docker
  ```bash
  docker run --name <image_name> -p <port>:3306 -e MYSQL_ROOT_PASSWORD=<password> mysql:5.7.36
  ```

- Manage database from Docker
  ```bash
  docker exec -it <container_id || image_name> bash
  ```
